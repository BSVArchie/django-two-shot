from django.forms import ModelForm, DateInput
from receipts.models import Receipt, ExpenseCategory, Account

class DateInput(DateInput):
    input_type = "date"

class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]

        # def __init__ (self, user, *args, **kwargs):
        #     super(ReceiptForm, self).__init__(*args, **kwargs)

        #     self.fields['category'].queryset = ExpenseCategory.objects.filter(owner=user)
        #     self.fields['account'].queryset = Account.objects.filter(owner=user)


class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]
